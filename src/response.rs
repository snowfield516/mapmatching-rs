use std::fmt;

use crate::osm::StreetNode;

/// The result of the map matching algorithm
#[derive(Debug)]
pub struct MapMatchingResponse {
    /// The resulting linestring. Every point in this line lays over a way or
    /// node in the street graph.
    pub path: Vec<StreetNode>,

    /// The cost of this solution
    pub cost: f64,
}

impl fmt::Display for MapMatchingResponse {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
