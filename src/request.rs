use std::str::FromStr;

use geo::LineString;
use crate::cost_function::CostFunction;

/// Used to limit how many nodes to process in a map matching request.
#[derive(Copy, Clone)]
pub enum NodeCount {
    /// All the nodes will be processed.
    All,

    /// Just some of the nodes will be processed
    Just(usize),
}

impl NodeCount {
    pub fn count(&self) -> usize {
        match *self {
            NodeCount::All => usize::max_value(),
            NodeCount::Just(some) => some,
        }
    }
}

impl FromStr for NodeCount {
    type Err=();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.parse::<usize>() {
            Ok(num) => Ok(NodeCount::Just(num)),
            Err(_) => Ok(NodeCount::All),
        }
    }
}

/// A request to match a gps track agains the street graph. Contains as fields
/// all the information needed for the calculation including parameters that
/// can be tuned.
pub struct MapMatchingRequest {
    /// The gps track to match to the streets.
    pub path: LineString<f64>,

    /// Radius in meters used to search from streets around each gps point.
    /// Larger values allow for better likelyhood of finding the right answer
    /// but also make this algorithm slower.
    pub radius: u64,

    /// Limits processing of this gps track to only some of the positions or
    /// all of them.
    pub node_count: NodeCount,

    /// The cost function to use given two adjacent gps position, two streets
    /// (one for each position) and the shortest path between each each street
    pub cost_function: Box<dyn CostFunction>,
}

#[cfg(test)]
mod tests {
    use super::NodeCount;

    #[test]
    fn test_node_count() {
        let count = NodeCount::All;
        assert_eq!(count.count(), usize::max_value());

        let count = NodeCount::Just(5);
        assert_eq!(count.count(), 5);
    }
}
