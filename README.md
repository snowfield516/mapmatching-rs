# Mapmatching-rs

A rewrite of [the same project](https://github.com/categulario/map_matching) from python to rust.

## Resources

This b-tree implementation will be used to replace the geohash (and thus the dependency on redis)

* https://docs.rs/rstar/0.7.1/rstar/

Line simplification with Ramer-Douglas-Peucker

## What to do

### Run the tests

`cargo test`

### Run the project

`cargo run -- sample.json`
